//
//  ViewController.swift
//  MSViewPagerController
//
//  Created by fauquette fred on 5/07/16.
//  Copyright © 2016 Agilys. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var viewPagerVC: MSViewPagerController?
    
    @IBOutlet weak var itemNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewPagerVC = segue.destination as? MSViewPagerController , segue.identifier == "viewPager" {
            viewPagerVC.pagerDelegate = self
            viewPagerVC.dataSource = self
            self.viewPagerVC = viewPagerVC
        }
    }
    
    @IBAction func selectItem(_ sender: AnyObject) {
        do {
            try viewPagerVC?.setSelectedIndex(Int(itemNumber.text ?? "0") ?? 0, animated: true)
        } catch MSViewPagerError.indexOutOfBound {
            print ("out of bound")
        } catch MSViewPagerError.indexAlreadySelected {
            print("index already selected")
        } catch {
            print("oups")
        }
        
    }
    
}

extension ViewController: MSViewPagerDelegate {
    func didSelectMSPage(_ viewPager: MSViewPagerProtocol, index: Int) {
        itemNumber.text = String(index)
    }
    func getTextColorMSPage(_ index: Int, state: UIControlState) -> UIColor? {
        if index == 2 {
            return UIColor.red
        }
        return nil
    }
    func getBarColorMSPage(_ index: Int) -> UIColor? {
        if index == 2 {
            return UIColor.red
        }
        return nil
    }
    func getBackGroundColorMSPage(_ index: Int, state: UIControlState) -> UIColor? {
        if index == 5 {
            return UIColor.red
        }
        return nil
    }
}

extension ViewController: MSViewPagerDataSource {
    func pageModelMSPage(_ index: Int) -> MSPageModelProtocol? {
        if index % 3 == 0 {
            return PageModel(title: "big huge main dynamic title\(index)")
        }
        return PageModel(title: "title\(index)")
    }
    var numberOfPagesMSPage: Int {
        return 10
    }
}

class PageModel: MSPageModelProtocol {
    @objc var title: String?
    @objc var icon: UIImage?
    init(title: String?) {
        self.title = title
    }
}

